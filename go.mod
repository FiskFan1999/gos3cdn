module codeberg.org/FiskFan1999/gos3cdn

go 1.19

require (
	github.com/aws/aws-sdk-go v1.44.72
	unit.nginx.org/go v0.0.0-20220805104717-b4797a4fcb2d
)

require github.com/jmespath/go-jmespath v0.4.0 // indirect
