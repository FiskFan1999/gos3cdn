package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	unit "unit.nginx.org/go"
)

var (
	bucket       string
	filename     string
	endpoint     string
	region       string
	accessKey    string
	accessSecret string
	profile      string
	port         int
)

/*
	bucket := "bucketname"
	filename := "filename"
	endpoint := "s3.us-west-1.wasabisys.com"
	region := "us-west-1"
	accessKey := "key"
	accessSecret := "secret"
	profile := "wasabi"
*/

func main() {

	flag.StringVar(&endpoint, "e", "", "API endpoint")
	flag.StringVar(&bucket, "b", "", "bucket name")
	flag.StringVar(&region, "r", "", "region (us-east-1, etc)")
	flag.StringVar(&accessKey, "k", "", "access key")
	flag.StringVar(&accessSecret, "s", "", "access key secret")
	flag.StringVar(&profile, "p", "", "profile (\"wasabi\" etc.)")
	flag.IntVar(&port, "l", 6060, "port to listen on (ignored if used as NGINX unit application)")

	flag.Parse()

	panic(unit.ListenAndServe(fmt.Sprintf(":%d", port), S3Handler(0)))
}

type S3Handler byte

func (h S3Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	filename := r.URL.Path

	s3Config := aws.Config{
		Credentials:      credentials.NewStaticCredentials(accessKey, accessSecret, ""),
		Endpoint:         aws.String(endpoint),
		Region:           aws.String(region),
		S3ForcePathStyle: aws.Bool(true),
	}

	goSession, err := session.NewSessionWithOptions(session.Options{
		Config:  s3Config,
		Profile: profile,
	})

	if err != nil {
		http.Error(w, "error during creation of session", 500)
		return
	}

	s3Client := s3.New(goSession)

	getObjectInput := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(filename),
	}

	var output *s3.GetObjectOutput
	output, err = s3Client.GetObject(getObjectInput)
	if err != nil {
		http.Error(w, "file not found", 404)
		return
	}

	defer output.Body.Close()
	// output.Body is not read seeker
	bodyBytes, err := io.ReadAll(output.Body)
	if err != nil {
		http.Error(w, "error while reading object", 500)
		return
	}
	bodyReader := bytes.NewReader(bodyBytes)
	http.ServeContent(w, r, filename, time.Now(), bodyReader)
}
